# Linux指令速查

## 1.Linux初步认识

### 1.1 Linux标识符

![在这里插入图片描述](https://gitee.com/li-jiagong/imgs/raw/master/Linux-imgs/Linux%E6%A0%87%E8%AF%86%E7%AC%A6.png)

### 1.2 Linux目录结构

![img](https://gitee.com/li-jiagong/imgs/raw/master/Linux-imgs/Linux%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84.jpg)

#### 1.2.1 (/)目录结构说明

|      目录       | 描述                                                         |
| :-------------: | ------------------------------------------------------------ |
|    ***/bin**    | **bin** 是 Binaries (二进制文件) 的缩写, 这个目录存放着最经常使用的命令。 |
|   ***/sbin**    | **sbin**是 Superuser Binaries (超级用户的二进制文件) 的缩写，这里存放的是系统管理员使用的系统管理程序。 |
|    ***/etc**    | **etc** 是 Etcetera(其他) 的缩写,这个目录用来存放所有的系统管理所需要的配置文件和子目录。 |
|    ***/opt**    | **opt** 是 optional(可选) 的缩写，这是给主机额外安装软件所摆放的目录。默认是空的。 |
|    ***/tmp**    | **tmp** 是 temporary(临时) 的缩写这个目录是用来存放一些临时文件的。 |
|    ***/usr**    | **usr** 是 unix shared resources(共享资源) 的缩写，这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于 windows 下的 program files 目录。 |
|    **/home**    | 用户的主目录，在 Linux 中，每个用户都有一个自己的目录。      |
|    **/root**    | 该目录为系统管理员，也称作超级权限者的用户主目录。           |
|    **/boot**    | 这里存放的是启动 Linux 时使用的一些核心文件，包括一些连接文件以及镜像文件。 |
|    **/dev **    | **dev** 是 Device(设备) 的缩写, 该目录下存放的是 Linux 的外部设备， |
|    **/lib**     | **lib** 是 Library(库) 的缩写这个目录里存放着系统最基本的动态连接共享库 |
| **/lost+found** | 这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件。 |
|   **/media**    | Linux 系统会自动识别一些设备，当识别后，Linux 会把识别的设备挂载到这个目录下。 |
|    **/mnt**     | 系统提供该目录是为了让用户临时挂载别的文件系统的。           |
|    **/proc**    | **proc** 是 Processes(进程) 的缩写，/proc 是一种伪文件系统（也即虚拟文件系统），存储的是当前内核运行状态的一系列特殊文件，这个目录是一个虚拟的目录，它是系统内存的映射，我们可以通过直接访问这个目录来获取系统信息。 |
|    **/srv**     | 该目录存放一些服务启动之后需要提取的数据。                   |

```shell
# 通过下面的命令来屏蔽主机的ping命令，使别人无法ping你的机器：
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
```

#### 1.2.2 目录颜色说明

![在这里插入图片描述](https://gitee.com/li-jiagong/imgs/raw/master/Linux-imgs/Linux%E7%9B%AE%E5%BD%95%E9%A2%9C%E8%89%B2%E8%AF%B4%E6%98%8E.png)

## 1. 帮助命令

### 1.1 help指令

- 指令的基本用法与选项介绍。

```shell
help
# 作用 ： 查看命令的帮助信息
# 格式 ： command --help
# 举例 ： ls --help 代表查看ls的帮助信息。 

```

### 1.2 man指令

- man 是 manual 的缩写，将指令的具体信息显示出来。

```shell
man
# 作用：查看命令的帮助信息
# 格式 ： man command
# 举例 ： man ls
```

## 2. 快捷操作

- **Tab**: 命令和文件名补全；
- **Ctrl+C**: 中断正在运行的程序；
- **Ctrl+D**: 结束键盘输入(End Of File，EOF)

## 3. 文件或目录的管理

### 3.1 处理目录的基本命令

|              命令               | 描述                                           |
| :-----------------------------: | ---------------------------------------------- |
|      **ls**（list files）       | 列出目录及文件名                               |
|   **cd**（change directory）    | 切换目录                                       |
| **pwd**（print work directory） | 显示目前的目录绝对路径                         |
|   **mkdir**（make directory）   | 创建一个新的目录                               |
|  **rmdir**（remove directory）  | 删除一个空的目录                               |
|       **cp**（copy file）       | 复制文件或目录                                 |
|       **mv**（move file）       | 移动（剪切）文件与目录，或修改文件与目录的名称 |
|        **rm**（remove）         | 删除文件或目录                                 |
|            **find**             | 查找文件或目录                                 |

#### 3.1.1 列出目录（ls）

```shell
# 语法：ls [-a/d/l] filename 
# -a ：全部的文件，连同隐藏文件( 开头为 . 的文件) 一起列出来(常用)
# -d ：仅列出目录本身，而不是列出目录内的文件数据(常用)
# -l ：长数据串列出，包含文件的属性与权限等等数据；(常用)
ls -a filename	
ls -d filename	#ls
ls -l filename	#ll
```

#### 3.1.2 切换目录（cd）

```shell
# 语法：cd [相对路径或绝对路径]
cd /dir
cd .. # 返回上级目录
```

#### 3.1.3 显示目录（pwd）

```shell
# 语法：pwd [-P]
# -P ：显示出确实的路径，而非使用连结 (link) 路径。
pwd
```

#### 3.1.4 创建目录（mkdir）

```shell
# 语法：mkdir [-m/p] 目录名称
# -m ：配置文件的权限喔！直接配置，不需要看默认权限 (umask) 的脸色～
# -p ：帮助你直接将所需要的目录(包含上一级目录)递归创建起来！
mkdir filename
mkdir -m 777 filename
mkdir -p /filename1/file/name
```

#### 3.1.5 删除目录（rmdir）

```shell
# 语法：rmdir [-p] 目录名称
# -p ：连同上一级『空的』目录也一起删除
rmdir filename
rmdir -p /filename1/file/name # 注意：所有目录都是空的
```

#### 3.1.6 复制文件或目录（cp）

```shell
# 语法：cp [-adfilprsu] 来源档(source) 目标档(destination)
# -a ：相当於 -pdr 的意思，至於 pdr 请参考下列说明；(常用)
# -p ：连同文件的属性一起复制过去，而非使用默认属性(备份常用)；
# -d ：若来源档为连结档的属性(link file)，则复制连结档属性而非文件本身；
# -r ：递归持续复制，用於目录的复制行为；(常用)
# -i ：若目标档(destination)已经存在时，在覆盖时会先询问动作的进行(常用)
# -f ：为强制(force)的意思，若目标文件已经存在且无法开启，则移除后再尝试一次；
# -l ：进行硬式连结(hard link)的连结档创建，而非复制文件本身；
# -s ：复制成为符号连结档 (symbolic link)，亦即『捷径』文件；
# -u ：若 destination 比 source 旧才升级 destination ！
cp [选项] source1 directory
```

#### 3.1.7 剪切文件或目录（mv）

```shell
# 语法：mv [-f/i/u] source destination
# -f ：force 强制的意思，如果目标文件已经存在，不会询问而直接覆盖；
# -i ：若目标文件 (destination) 已经存在时，就会询问是否覆盖！(默认)
# -u ：若目标文件已经存在，且 source 比较新，才会升级 (update)
mv [选项] source1 source2 source3 .... directory
```

#### 3.1.8 删除文件或目录（rm）

```shell
# 语法：rm [-f/i/r] 文件或目录
# -r ：递归删除啊！最常用在目录的删除了！这是非常危险的选项！！！
# -f ：就是 force 的意思，忽略不存在的文件，不会出现警告信息；
# -i ：互动模式，在删除前会询问使用者是否动作
rm [选项] filename
```

#### 3.1.9 查找文件或目录（find）

```shell
# 语法：find [path] [option] [pattern]
# [path] 路径 [option] 选项 [pattern] 匹配模式（文件名）
# [option] -> -name -type -size
find ./ -name '*.txt'
find ./ -type b/-
find ./ -size 1M
```

#### 3.1.10 通配符

```shell
# 通配符
# * 匹配任意字符
# . 匹配单个字符
# [a-z] 匹配中括号的字符
```

### 3.2 处理文件的基本命令

|   命令    | 描述                                                 |
| :-------: | ---------------------------------------------------- |
| **touch** | 创建一个文件                                         |
|  **cat**  | 由第一行开始显示文件内容                             |
| **echo**  | 输出简短的文本消息或将变量值输出到控制台             |
|  **tac**  | 从最后一行开始显示，可以看出 tac 是 cat 的倒着写！   |
|  **nl**   | 显示的时候，顺道输出行号！                           |
| **more**  | 一页一页的显示文件内容                               |
| **less**  | 与 more 类似，但是比 more 更好的是，他可以往前翻页！ |
| **head**  | 只看头几行                                           |
| **tail**  | 只看尾巴几行                                         |

#### 3.2.1 创建文件（touch）

```shell
# 语法：touch 文件
touch filename
```

#### 3.2.2 正查看文件（cat）

```shell
# 语法：cat [-A/b/E/n/T/v] 文件
# -A  ：相当於 -vET 的整合选项，可列出一些特殊字符而不是空白而已；
# -v  ：列出一些看不出来的特殊字符
# -E  ：将结尾的断行字节 $ 显示出来；
# -T  ：将 [tab] 按键以 ^I 显示出来；
# -b  ：列出行号，仅针对非空白行做行号显示，空白行不标行号！
# -n  ：列印出行号，连同空白行也会有行号，与 -b 的选项不同；
cat [-A/b/E/n/T/v] filename
```

#### 3.2.3 输出信息（echo）

```shell
# 语法：echo [info] >/>> filename
echo "hello world" > a.txt # 用info覆盖a.txt文件
echo "hello world" >> a.txt #给a.txt追加info
```

#### 3.2.4 倒查看文件（tac）

```shell
# 语法：tac 文件
tac filename
```

#### 3.2.5 输出行号（nl）

```shell
# 语法：nl 文件
nl filename
```

#### 3.2.6 分页显示文件一（more）

```shell
# 语法：more 文件
more filename
```

#### 3.2.7 分页显示文件二（less）

```shell
# 语法：less 文件
less filename
```

#### 3.2.8 查看文件头（head）

```shell
# 语法：head [-n number] 文件
# 默认查看 10 行
head -n 50 filename
```

#### 3.2.9 查看文件尾（tail）

```shell
# 语法：tail [-n number] 文件
# 默认查看 10 行
tail -n 50 filename
```

#### 3.2.10 文本过滤（grep）

```shell
# 语法：grep [option] [pattern] filename
# [option] -c : 统计个数 -i : 忽略大小写 -n : 加上行号 -v : 取反
# [pattern] 文本或正则表达式
#
grep -c ".*llo" a.txt 
grep -i "Hello WORLD" a.txt
grep -n "^h" a.txt # 以h开头
grep -v "o$" a.txt # 以o结尾
```

## 4. 文件或目录的属性

### 4.1 文件属性

> 在 Linux 中**第一个字符**代表这个文件是目录、文件或链接文件等等。

- 当为 d 则是目录
- 当为 - 则是文件；
- 若是 l 则表示为链接文档(link file)；
- 若是 b 则表示为装置文件里面的可供储存的接口设备(可随机存取装置)；
- 若是 c 则表示为装置文件里面的串行端口设备，例如键盘、鼠标(一次性读取装置)。

### 4.2 修改所属用户名或组（chown&chgrp）

#### 4.2.1 修改用户名（chown）

```shell
# 语法一：chown [–R] 属主名 文件名
# 语法二：chown [–R] 属主名：属组名 文件名
# -R ：递归更改文件属组。
chown -R name filename
chown -R name:groupname filename
```

#### 4.2.2 修改用户组（chgrp）

```shell
# 语法一：chgrp [-R] 属组名 文件名
chgrp -R groupname filename
```

### 4.3 修改用户权限（chmod）

- Linux文件属性有两种设置方法，一种是数字，一种是符号。
  - r : 4
  - w : 2
  - x : 1

- Linux 文件的基本权限就有九个，分别是 **user/group/others(拥有者/组/其他)** 三种身份各有自己的 **read/write/execute** 权限。
  - **user** （拥有者）	
  - **group** （组）
  - **others**（其他）

|   方法    | 参数 | 运算符 | 参数 | 文件或目录 |
| :-------: | :--: | :----: | :--: | :--------: |
|           |  u   |   +    |  r   |            |
| **chmod** |  g   |   -    |  w   |  file&dir  |
|           |  o   |   =    |  x   |            |
|           |  a   |        |      |            |

```shell
# 语法一：chmod 421 文件名
# 语法二：chmod u=rwx,g=rx,o=r 文件名
chmod 421 filename
chmod u=rwx,g=rx,o=r filename
# 给三者都增加了r（读）权限
chmod a+r filename
```

## 5. vi/vim文档编译器

**vi/vim** 共分为三种模式，分别是**命令模式（Command mode）**，**输入模式（Insert mode）**和**底线命令模式（Last line mode）**。

![img](https://gitee.com/li-jiagong/imgs/raw/master/Linux-imgs/Vi-Vim%E9%94%AE%E7%9B%98%E5%9B%BE.png)

### 5.1 命令模式

启动 vi/vim，便进入了命令模式。

以下是常用的几个命令：

- **i** 切换到输入模式，以输入字符。
- **x** 删除当前光标所在处的字符。
- **:** 切换到底线命令模式，以在最底一行输入命令。

|    命令     | 描述                                                         |
| :---------: | ------------------------------------------------------------ |
|  **i, I**   | **进入输入模式(Insert mode)：**<br/>i 为[从目前光标所在处输入]，<br/> I 为[在目前所在行的第一个非空格符处开始输入]。 |
|  **a, A**   | **进入输入模式(Insert mode)：**<br/>a 为[从目前光标所在的下一个字符处开始输入]，<br/> A 为[从光标所在行的最后一个字符处开始输入]。 |
|  **o, O**   | **进入输入模式(Insert mode)：**<br/>o 为在目前光标所在的下一行处输入新的一行；<br/> O 为在目前光标所在的上一行处输入新的一行！ |
|  **r, R**   | **进入取代模式(Replace mode)：**<br/>r 只会取代光标所在的那一个字符一次；<br/>R会一直取代光标所在的文字，直到按下 ESC 为止； |
|  **x, X**   | 在一行字当中，<br/>x 为向后删除一个字符 (相当于 [del] 按键)<br> X 为向前删除一个字符(相当于 [backspace] 亦即是退格键) |
|   **dd**    | 删除游标所在的那一整行                                       |
|   **yy**    | 复制游标所在的那一行(常用)                                   |
|  **p, P**   | p 为将已复制的数据在光标下一行贴上，P 则为贴在游标上一行！   |
|    **G**    | 移动到这个档案的最后一行(常用)                               |
|    **u**    | 对输入的内容进行撤销                                         |
| **ctrl+r**  | 对已经撤销的内容进行恢复                                     |
| **/字符串** | 要查找的字符串，若向下按n，向上按N。                         |

### 5.2 输入模式

- 略

### 5.3 底线命令输入模式

在底线命令模式中，基本的命令有（已经省略了冒号）：

- ！：强制模式
- q ：退出程序
- w ：保存文件

按ESC键可随时退出底线命令模式。

|     命令      | 描述                                               |
| :-----------: | -------------------------------------------------- |
| :w [filename] | 将编辑的数据储存成另一个档案（另存）               |
| :r [filename] | 在编辑的数据中，读入另一个档案的数据。             |
|    :set nu    | 显示行号，设定之后，会在每一行的前缀显示该行的行号 |
|   :set nonu   | 与 set nu 相反，为取消行号！                       |

## 6. 指令与文件搜索

### 6.1 指令搜索（which）

```shell
# 语法：which [-a] 命令
# -a : 将所有指令列出，而不是只列第一个
which [-a] command
```

### 6.2 文件搜索（whereis）

```shell
# 语法：whereis [-bmsu] dirname/filename
# find [basedir] -name dirname/filename
whereis 'shadow*'
```

### 6.3 索引搜索（locate）

```shell
# 语法：locate 文件名
# 1. 先更新数据库
updatedb
locate filename
```

## 7. 压缩与解压缩

### 7.1 gzip

```shell
# 语法：gzip [-d] filename
# 选项:  -d : 解压 -h : 帮助 -v : 显示指令执行过程
gzip filename #压缩
gzip -d filename.gz #解压
gzip -h #显示帮助信息
gzip -v filename #显示压缩信息
```

### 7.2 zip,unzip

```shell
# 压缩
# 语法：zip [-r] 文件.zip 源文件
# 选项: -r 递归处理，将指定目录下的所有文件和子目录一并处理
zip -r file.zip a.txt

# 解压缩
# 语法：unzip 文件.zip 
# 选项: -l : 显示压缩文件内所包含的文件 -v : 显示指令执行过程
unzip -l file.zip #查看压缩文件中包含的文件
unzip -v file.zip #用于查看压缩文件目录信息，但是不解压该文件
```

### 7.3 tar

```shell
# 打包 储存很大
# 语法：tar [-c/x/v/f/t/z] 文件 源文件
# 选项： -z : 使用 zip ; -j : 使用 bzip2 ; -J : 使用 xz ; -c : 创建打包文件 -x : 解包打包文件 -v : 查看打包的过程 -f : 要处理的文件 -t : 查看包的内容 
tar -cvf file.tar ./a.txt
tar -xvf file.tar
tar -tvf file.tar
tar -zxvf file.gz #压缩
```

## 8.系统命令

### 8.1 查看磁盘（df）

```shell
# 语法：df [-h] [目录或文件夹]
# 选项：-h : 以人们较易阅读的 GB, MB, KB等格式自行显示
df -h [dir/file]
```

### 8.2 管理和查看分区（fdisk）

```shell
# 语法：fdisk [-l] 
# 选项：-l : 系统将会把整个系统内能够搜寻到的装置的分区均列出来
fdisk -l
```

### 8.3 查看文件大小（du）

```shell
# 语法：du [-h] 文件或目录
# 选项：-h : 以人们较易阅读的 GB, MB, KB等格式自行显示
du -h filename
```

### 8.4 查看进程对应的CPU和内存占用率（top）

```shell
# 语法：top [-p] 进程号
# 选项：-p : 显示对应 进程号 的进程信息
top #显示进程信息
top -p 1100 #显示指定的进程信息
```

### 8.5 查看内存大小（free）

```shell
# 语法：free
# 选项： -h : 以人们较易阅读的 GB, MB, KB等格式自行显示 -m : 以MB为单位显示内存使用情况。
free -h
```

### 8.6 查看系统中进程（ps）

```shell
# 语法：ps [-a/u/x/e/f] 
# 选项：-aux : 显示所有用户的所有进程信息。 
# ***  -ef : 显示所有用户的所有进程信息。(优先用)
# 可以用grep进行过滤
ps -aux | grep ^h
ps -ef 
```

### 8.9 杀掉进程（kill）

```shell
# 语法：kill [-9] 进程号
# 选项：-9 : 强制杀掉进程
kill -9 PID
```

### 8.10 查看ip地址（ifconfig）

```shell
# 语法：ifconfig
ifconfig
```

### 8.11 检查网络（ping）

```shell
# 语法：ping ip地址
ping 192.168.2.1
```

### 8.12  查看端口（netstat）

```shell
# 语法：netstat [-a/n/p]
# 选项：-a(all) : 显示所有连线中的Socket -n(numeric) : 直接使用IP地址，而不通过域名服务器 -p(programs) : 显示正在使用Socket的程序识别码和程序名称
netstat -anp | grep 1001
```

## 9. 用户操作

### 9.1 切换用户（su）

```shell
# 语法：su [-] 用户名
su username
su - username # 到用户的家目录下
```

### 9.2 创建用户（useradd）

```shell
# 语法：useradd [-d] 用户名
# 选项：-d : 创建用户目录
useradd -d username
```

### 9.3 设置密码（passwd）

```shell
# 语法：passwd 用户名
passwd username
```

### 9.4 删除用户（userdel）

```shell
# 语法：userdel [-r/f] 用户名
# 选项：-r : 递归删除啊！ -f : force强制删除
userdel -rf username
```

### 9.5 查看用户（whoami）

```shell
# 语法：whoami -全称是"Who am I?”，意为“我是谁？”
whoami # 查看当前登录用户
```

## 10. 网络配置

### 10.1 VMware提供了三种网络连接模式：

- 桥接模式：
  - 虚拟机直接连接外部物理网络的模式，主机起到了网桥的作用。这种模式下，虚拟机可以直接访问外部网络，并且对外部网络是可见的。
- NAT模式：VMnet8
  - 虚拟机和主机构建一个专用网络，并通过虚拟机网络地址转换（**NAT**）设备对**IP**进行转换。虚拟机通过共享主机**IP**可以访问外部网络，但外部网络无法访问虚拟机。
  - **虚拟机的子网**是192.168.100.0，**NAT的网关**是192.168.100.2，**主机的网关**是192.168.100.1。
- 仅主机模式：VMnet1
  - 虚拟机只与主机共享一个专用网络，与外部网络无法通信。

### 10.2 关闭防火墙

```shell
在CentOS 7上，可以使用以下命令来关闭防火墙：
1. 停止firewalld服：
systemctl stop firewalld
```

```shell
2. 禁用firewalld服务，以防止系统重启后此服务再次启动:
systemctl disable firewalld
```

```shell
3. 查看firewalld服务(防火墙规则)：
firewall-cmd --list-all
```

### 10.2 设置网络通信

#### 10.2.1 使用`nmcli`命令修改网络

- 1.首先列出当前可用的网络连接。

```shell
nmcli connection show
# 在输出结果中找到您想要修改的网络连接的名字，比如`ens33`等。
```

- 2.使用`nmcli`命令来修改该网络连接的参数。DHCP IP(自动）

```shell
nmcli connection modify "ens33" ipv4.method auto
```

- 3.将网络连接设置为静态IP

```shell
nmcli connection modify "ens33" ipv4.method manual ipv4.addresses 192.168.100.100/24 ipv4.gateway 192.168.100.2 ipv4.dns 192.168.100.2
# `ipv4.addresses`是您要设置的静态IP地址，`ipv4.gateway`是您的网关IP地址，`ipv4.dns`是您的DNS服务器IP地址。
```

- 4.重新启用该网络连接

```shell
nmcli connection up "ens33"
```

### 10.3 网络设置（nmcli）

- "nmcli"是一个在命令行下用于管理和配置NetworkManager的工具。
- 以下是一些常用的nmcli命令：
  1. 显示NetworkManager的状态：`nmcli general status`
  2. 连接WiFi网络：`nmcli device wifi connect SSID-Name password WIFI_Password`
  3. 显示当前连接的网络：`nmcli connection show --active`
  4. 添加VPN连接：`nmcli connection import type openvpn file myvpn-config.ovpn`
  5. 显示所有可用连接：`nmcli connection show`
  6. 断开当前连接：`nmcli connection down Connection-Name`
  7. 启用和禁用网络适配器：`nmcli radio wifi on/off`
  8. 显示当前连接的详细信息：`nmcli connection show Connection-Name`



## 11. 软件包管理

### 11.1 语法（yum）

| 命令 |         选项         |     操作     | 描述                          | 包名     |
| :--: | :------------------: | :----------: | ----------------------------- | :------- |
|      |                      |    search    | 搜索软件包信息                |          |
|      |                      |   install    | 安装软件包                    |          |
|      |      -h（帮助）      |    remove    | 删除软件包                    |          |
| yum  |  -y（安装全程yes）   |     list     | 显示软件包信息                | filename |
|      | -q（不显示安装过程） |    update    | 更新软件包                    |          |
|      |                      | check-update | 检查是否有可用的更新软件包    |          |
|      |                      |    clean     | 清理 yum 过期的缓存           |          |
|      |                      |   deplist    | 显示 yum 软件包的所有依赖关系 |          |

### 11.2 常用命令

- 1.列出所有可更新的软件清单命令：yum check-update

- 2.更新所有软件命令：yum update
- 3.仅安装指定的软件命令：yum install <package_name>
- 4.仅更新指定的软件命令：yum update <package_name>
- 5.列出所有可安裝的软件清单命令：yum list
- 6.删除软件包命令：yum remove <package_name>
- 7.查找软件包命令：yum search <keyword>

### 11.3 国内yum镜像设置

[centos Linux 源使用帮助]: https://lug.ustc.edu.cn/wiki/mirrors/help/centos
[Rocky Linux 源使用帮助]: https://mirrors.ustc.edu.cn/help/rocky.html

```shell
# 1）安装 wget, wget 用来从指定的 URL 下载文件
[root@test ~] yum install wget
# 2）在/etc/yum.repos.d/目录下，备份默认的 repos 文件
[root@test yum.repos.d] cp CentOS-Base.repo CentOS-Base.repo.backup
# 3）下载网易 163 或者是 aliyun 的 repos 文件,任选其一，如图 8-2
[root@test yum.repos.d] wget http://mirrors.aliyun.com/repo/Centos-7.repo //阿里云
[root@test yum.repos.d] wget http://mirrors.163.com/.help/CentOS7-Base-163.repo //网易 163
# 4）使用下载好的 repos 文件替换默认的 repos 文件
[root@test yum.repos.d]\# mv CentOS7-Base-163.repo CentOS-Base.repo
# 5）清理旧缓存数据，缓存新数据
[root@test yum.repos.d]\#yum clean all
[root@test yum.repos.d]\#yum makecache
```

## 注意：

>网络配置和yum仓库配置看不懂的可以去看这个视频：
>[3天搞定Linux，1天搞定Shell，清华学神带你通关](https://www.bilibili.com/video/BV1WY4y1H7d3/?p=19&amp;share_source=copy_web&amp;vd_source=7e6e8d8e17447731b69d9a0a75c0c38)

# 参考资料：

[菜鸟教程](https://www.cainiaojc.com/linux/)

