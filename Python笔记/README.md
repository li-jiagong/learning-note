
# Python概览表

| 序号 | 章节             | 描述                                                         |
| :--: | :--------------- | :----------------------------------------------------------- |
|  1   | Python基础       | 1.认识Python<br>2.注释，变量，变量类型，输入输出，运算符     |
|  2   | 流程控制结构     | 1.判断语句<br>2.循环                                         |
|  3   | 数据序列         | 1.字符串（str）<br>2.列表（list）<br>3.元组（tuple）<br>4.字典（dict）<br>5.集合（set） |
|  4   | 函数             | 1.函数基础<br/>2.变量进阶<br/>3.函数进阶<br/>4.匿名函数      |
|  5   | 面向对象         | 1.面向对象编程<br/>2.类和对象<br/>3.面向对象基础语法<br/>4.封装，继承，多态<br/>5.类属性和类方法 |
|  6   | 异常，模块，文件 | 1.异常<br/>2.模块和包<br/>3.文件操作                         |
|  7   | Python进阶知识   | 1.迭代器（Iterator）<br>2.生成器（Yield）                    |

# 1. Python基础

## 1.1 认识Python

- Python是一种跨平台的计算机程序设计语言。是一种面向对象的动态类型语言，最初被设计用于编写自动化脚本(shell)，随着版本的不断更新和语言新功能的添加，越多被用于独立的、大型项目的开发。

## 1.2 注释

- 单行注释

```python
# 这是一个单行注释
# 一般是要空一格开始写。
```

- 多行注释

```python
"""双引号多行注释"""
'''单引号多行注释'''
"""一般用于函数中描述"""
```

## 1.3 变量

### 1.3.1 变量名的规范

1. 变量名只能包含字母，数字和下划线。不能以数字开头。
2. 变量名不能包含空格，但能使用下划线分隔单词。
3. 不能将Python关键字和函数名用作变量名。
4. 变量名应简短且具有描述性。
5. 慎用小写字母 l 和大写字母 O ，因为它们可能被人看错成数字。

### 1.3.2 变量的类型

- 数字类型

| 类型     | 变量名 | 描述         |
| :------- | ------ | ------------ |
| 整型     | int    | 1            |
| 浮点型   | float  | 1.0          |
| 布尔类型 | bool   | True / False |

- 非数字类型

| 类型   | 变量名 | 描述                     |
| ------ | ------ | ------------------------ |
| 字符串 | str    | ‘李四’                   |
| 列表   | list   | [1,2,3]                  |
| 元组   | tuple  | (1,2,3)                  |
| 字典   | dict   | {1 : '李四', 2 : '张三'} |

### 1.3.3 变量类型转换

| 类型   | 方法     | 描述                                                         |
| :----- | -------- | ------------------------------------------------------------ |
| 整形   | int( )   | 1.将`float`类型转为整数型<br>2.将`整数类型的字符串`转为整数型 |
| 浮点型 | float( ) | 1.将`int`类型转为浮点型<br/>2.将`数字类型的字符串`转为浮点型 |
| 字符串 | str( )   | 将其他类型转换为字符串                                       |

## 1.4 输入（input( )）

```python
# 获取用户使用键盘录入的内容
变量 = input('请输入内容：')
```

## 1.5 输出（print( )）

```python
# 将程序中的数据或者结果打印到控制台(屏幕)
print("hello world")
```

### 1.5.1 格式化输出

```yacas
在字符串中指定的位置，输出变量中存储的值。
1.在需要使用变量的地方，使用特殊符号占位。
2.使用变量填充占位的数据。
```

- **%** 格式化输出占位符号
  - `%d`  占位, 填充 整型数据  `digit`
  - `%f`  占位. 填充  浮点型数据  `float`
  - `%f`  占位. 填充  字符串数据  `str`

```python
# 定义变量 姓名  年龄  身高
name = '李四'  
age = 20   
height = 1.75

# 使用格式化输出实现
# print('我的名字是 name, 年龄是 age, 身高是 height m')
print('我的名字是 %s, 年龄是 %d, 身高是 %f m' % (name,age,height))
# 小数默认显示 6 位, 如果想要指定显示小数点后几位,  %.nf , n 需要换成具体的整数数字,即保留小数的位置
```

- **F-string**( f字符串的格式化方法)

```yacas
f-string 格式化的方法,想要使用 ,Python 的版本 >= 3.6
1. 需要在字符串的前边加上 f"" 或者 F""
2. 占位符号统一变为 {} 
3. 需要填充的变量 写在 {} 中
```

```python
# 定义变量 姓名  年龄  身高
name = '李四'  
age = 20   
height = 1.75
# 使用 F 格式化输出实现
# print('我的名字是 name, 年龄是 age, 身高是 height m')
print("我的名字是 {name}, 年龄是 {age}, 身高是 {height} m")
```

## 1.6 快捷键(小操作)

```yacas
撤销 : Ctrl Z 
删除一行: Ctrl x
复制粘贴一行: Ctrl d 
```

## 1.7 运算符

### 1.7.1 算术运算符

| 运算符 | 描述             | 实例        |
| :----: | ---------------- | ----------- |
| **+**  | 加               | x + y = 3   |
| **-**  | 减               | x - y = 1   |
|  *  | 乘               | x * y = 10  |
| **/**  | 除               | x + y = 0.5 |
| **//** | 求商             | x // y = 2  |
| **%**  | 求余             | x % y = 1   |
| ** | 幂运算，指数运算 | x ** 3 = 8  |

### 1.7.2 比较运算符

| 运算符 | 描述     | 实例   |
| :----: | -------- | ------ |
| **>**  | 大于     | x > 3  |
| **<**  | 小于     | x < 3  |
| **>=** | 大于等于 | x >= 3 |
| **<=** | 小于等于 | x <= 3 |
| **==** | 等于     | x == 3 |
| **!=** | 不等于   | x != 3 |

### 1.7.3 逻辑运算符

| 运算符  | 描述   | 实例     |
| :-----: | ------ | -------- |
| **and** | 逻辑与 | 一假为假 |
| **or**  | 逻辑或 | 一真为真 |
| **not** | 逻辑非 | 取反     |

### 1.7.4 成员运算符

|   运算符   | 描述                            | 实例          |
| :--------: | ------------------------------- | ------------- |
|   **in**   | 如果在序列中找到值/变量，则为真 | 5 in list     |
| **not in** | 如果在序列中找到值/变量，则为真 | 5 not in list |

### 1.7.5 身份运算符

|   运算符   | 描述                                               | 实例       |
| :--------: | -------------------------------------------------- | ---------- |
|   **is**   | 判断俩个变量是否引用同一个地址值，且数值相同       | a is b     |
| **not is** | 判断俩个变量是否引用同一个地址值，且数值不相同相同 | a not is b |

# 2. 流程控制结构

## 2.1 判断语句

### 2.1.1 if 结构

```python
if 判断条件:
    书写条件成立(真),执行的代码
    书写条件成立(真),执行的代码
print("i like python")

# 顶格书写,没有缩进的代码,和 if 无关, 不管条件是否成立,都会执行
```

### 2.1.2 if elif else 结构

```python
if 判断条件1:
    判断条件1成立,执行的代码
elif 判断条件2:  # 只有判断条件1不成立,才会判断 判断条件2
    判断条件2成立执行的代码
else:
    以上条件都不成立,执行的代码
```

### 2.1.3 if else 结构

```python
if 判断条件:
    书写条件成立(真),执行的代码
    书写条件成立(真),执行的代码
else:
    书写条件不成立(假), 执行的代码
    书写条件不成立(假), 执行的代码
```

### 2.1.4 三元表达式

```python
# 语法
结果 if 条件 else 结果
# 例如
c = a if a > b else b
```

## 2.2 循环

### 2.2.1 for 循环

```python
for 变量名 in 容器:
    重复执行的代码

# 1. for 和 in 都是关键字。
# 2. 容器中有多少个数据，循环会执行多少次(0 个数据,执行 0 次, ...)。
# 3. 每次循环，会将容器中数据取出一个保存到 in 关键字前边的变量中。
```

### 2.2.2 数据的构建（range）

```python
# 语法: range(start_index,end_index,len)
# start_index: 开始下标; end_index: 结束下标（不包含）;len: 步长。
for value in range(,5,2)
	print(value)
    
list_numbers = list(range(1,5,2))
print(list_numbers)
```

### 2.2.2 while 循环

```python
index = 0
while index < 5:
    print(index)
    index += 1
# 相当于 Java 中的 for 循环
```

### 2.2.2 循环终结（continue - break - pass）

```python
break: 终止循环, 即代码执行遇到 break, 循环不再执行,立即结束。
continue: 跳过本次循环. 即代码执行遇到 continue,本次循环剩下的代码不再执行, 继续下一次循环。
pass: 执行传递时没有任何反应，结果为无操作（NOP）。
```

# 3. 数据序列

## 3.1 字符串（str）

### 3.1.1 定义

- 使用**引号**(单引号, 双引号, 三引号)引起来的内容就是字符串

### 3.1.2 下标

- **下标(索引)**: 就是指字符在字符串中的位置编号, 这个编号就是下标

```python
# 语法: 
str[index]  # 获取指定位置的字符
```

### 3.1.3 字符串切片

```python
# 切片: 可以获取字符串中多个字符(多个字符的下标是有规律的, 等差数列)
# 语法:
str[start:end:step] # start:开始位置（从0起步）end:结束位置 step:步长
str[0:2:2]
```

### 3.1.4 成员方法

| 成员方法                                      | 描述                       | 实例                   |
| :-------------------------------------------- | -------------------------- | ---------------------- |
| **find( string, start_index = 0 )**           | 在字符串中查找“指定字符”。 | str.find( 'lisi' )     |
| **replace( old_str, new_str, count = 全部 )** | 替换字符串中的“指定字符”。 | str.replace( 'g', 'G ) |
| **split( string, count = 全部 )**             | 按照“指定字符”拆分字符串   | str.split( ',' )       |
| **join( string )**                            | 将字符串和指定字符串链接   | '-'.join(str)          |
| **title()**                                   | 将字符串的首字母大写       | str.title()            |
| **upper()**                                   | 将字符串进行全部大写       | str.upper()            |
| **lower()**                                   | 将字符串进行全部小写       | str.lower()            |
| **strip()**                                   | 将字符串的空白删除         | str.strip()            |
| **rstrip()**                                  | 将字符串的右空白删除       | str.rstrip()           |
| **lstrip()**                                  | 将字符串的左空白删除       | str.lstrip()           |

## 3.2 列表（list）

### 3.2.1 定义

- 列表的特点：可重复性，有下标性，可扩展性。

### 3.2.2 列表切片

```python
# 切片: 可以获取字符串中多个字符(多个字符的下标是有规律的, 等差数列)
# 语法:
list[start:end] # start:开始位置（从0起步）end:结束位置
str[0:5]
```

### 3.2.3 成员方法

| 成员方法                                 | 描述                                                   | 实例                          |
| ---------------------------------------- | ------------------------------------------------------ | ----------------------------- |
| **append()**                             | 在列表末尾添加一个数据                                 | list.append('数据')           |
| **insert(index, 元素)**                  | 在列表index下标上添加“指定元素”                        | list.insert(0, '元素')        |
| **del list[index]**                      | 根据列表下标索引来删除元素                             | del list[0]                   |
| **pop(index = -1)**                      | 1.把列表末尾的数据抛出<br>2.根据列表下标索引来抛出元素 | 1.list.pop()<br>2.list.pop(0) |
| **remove()**                             | 根据“指定元素”删除列表中的值                           | list.remove('元素')           |
| **clear()**                              | 清空列表内容                                           | list.clear()                  |
| **index('元素', start_index,end_index)** | 查找“指定元素”第一次出现的下标值                       | list.index('元素')            |
| **count('元素')**                        | 查找“指定元素”出现的个数                               | list.count('元素')            |
| **copy()**                               | 浅克隆一个列表，地址值一样                             | list.copy()                   |
| **reverse()**                            | 反转列表                                               | list.reverse()                |
| **sort(reverse=True)**                   | 列表排序                                               | list.sort()                   |
| **sorted()**                             | 列表临时排序，不改变原列表                             | list.sorted()                 |

### 3.2.4 列表解析 *

```python
# 语法
运算表达式 循环体 循环后的表达式
# 一
[x**2 for i in range(10)]
# 二
[x**2 for i in range(10) if x % 3 == 0]
# 三
[x**2 for i in range(1,10) for j in range(1,10)]
```

### 3.2.5 嵌套列表浅拷贝和深拷贝

- **拷贝**会改变**元素地址值**

- **浅拷贝**：只拷贝原列表的最外层列表，而内层列表依然指向原列表创建时的内存地址。

| 成员方法 | 描述                                       | 实例        |
| -------- | ------------------------------------------ | ----------- |
| copy()   | 浅克隆一个嵌套列表，子对象元素地址值一样。 | list.copy() |

- **深拷贝**：将外层列表和内层列表同时拷贝。

| 成员方法   | 描述                                                       | 实例            |
| ---------- | ---------------------------------------------------------- | --------------- |
| deepcopy() | 深克隆一个嵌套列表，元素地址值都不相同。（需要导入copy包） | copy.deepcopy() |

## 3.3 元组（tuple）

### 3.3.1 定义

- 元组的特点：可重复性，有下标性，数据不可变，不可扩展性。
- 应用: 在函数的传参或者返回值中使用, 保证数据不会被修改

### 3.3.2 成员方法

| 成员方法                                 | 描述                             | 实例                |
| ---------------------------------------- | -------------------------------- | ------------------- |
| **index('元素', start_index,end_index)** | 查找“指定元素”第一次出现的下标值 | tuple.index('元素') |
| **count('元素')**                        | 查找“指定元素”出现的个数         | tuple.count('元素') |

### 3.3.3 枚举（enumerate）

```python
# enumerate( ) 把列表中的每个值转换为元组（0，‘元素’）
lis = ['mon', 'tur', 'sun']
# 遍历
for i in enumerate(lst):
    print(i)
```

## 3.4 字典（dict）

### 3.4.1 定义

- Python字典是一个无序的项集合。其他复合数据类型只有值作为元素，而字典有一个键:值对（key：value）。

### 3.4.2 成员方法

| 成员方法          | 描述                         | 实例            |
| ----------------- | ---------------------------- | --------------- |
| **del dict[key]** | 根据字典“键值”来删除元素     | del dict['key'] |
| **pop[key]**      | 根据字典“键值”来抛出元素     | dict.pop['key'] |
| **clear()**       | 清空字典的键值对             | dict.clear()    |
| **get(key)**      | 根据“键值“获取字典中对应的值 | dict.get['key'] |

### 3.4.3 字典的遍历（3种）

```python
# 1. 遍历字典的键
for k in my_dict.keys():
    print(k)

# 2. 遍历字典的值
for v in my_dict.values():
    print(v)

# 3. 遍历键值
for k, v in my_dict.items():
    print(k, v)
```

### 3.4.4 内置函数（zip）

```python
# zip( ) 函数用于把俩个列表组合起来转换为字典
lis1 = ['name','age','day']
lis2 = ['lisi', 12 , 30]
# 第一种方法
for i in zip(lis1,lis2):
    print(f"{i}")
# 第二种方法
lis = list(zip(lis1,lis2))
print(lis)
```

## 3.5 集合（set）

### 3.5.1 定义

- 集合是项目的无序集合。每个元素都是唯一的（没有重复项），只能增加或删除元素（不能更改元素）。

### 3.5.2 成员方法

| **成员方法**     | **描述**                     | **实例**           |
| ---------------- | ---------------------------- | ------------------ |
| **add(元素)**    | 在集合末尾添加一个数据       | set.add(元素)      |
| **remove(元素)** | 根据“指定元素”删除集合中的值 | set.remove('元素') |
| 交集 &           | 集合相交                     | set1 & set2        |
| 并集 \|          | 集合并集                     | set1 \| set2       |
| 差集 -           | 集合相差                     | set1 - set2        |

# 4. 函数

## 4.1 函数的定义

- 函数是一组执行特定任务的相关语句。

```python
def function_name(parameters):
	"""docstring"""
	print(parameters)
    return parameters
```

## 4.2 函数的参数

### 4.2.1 默认参数

```python
def greet(name, msg="早上好!"):
    """
    此函数向提供消息的人打招呼。
    如果没有提供消息，
    则默认为“早上好!”
    """
    print("你好", name + ', ' + msg)
```

### 4.2.2 关键字参数

```python
def greet(name, msg):
    print("你好", name + ', ' + msg)
    
greet(name='lisi',msg='你好啊!')
greet(msg='你好啊!',name='lisi')
greet('lisi',msg='你好啊!')
```

### 4.2.3 任意参数（2种）

- 不定长元组参数

```python
# 不定长元组参数
def greet(*args):l;kl  
    """此函数被打招呼的对象为
        元组列表中的所有人."""
    for arg in args:
        print("Hello", arg)
# 名称是一个带有参数的元组
greet("Monica", "Luke", "Steve", "John")
greet(*("Monica", "Luke", "Steve", "John"))
```

- 不定长字典参数

```python
# 不定长字典参数
def greet(**kwargs):
    """此函数被打招呼的对象为
        元组列表中的所有人."""
    for key in kwargs.keys():
        print(kwargs[key])
# 名称是一个带有参数的字典
greet(a=1, b=2, c=3)
greet(a=1, b=2, c=3)
```

## 4.3 闭包函数

### 4.3.1 定义

- 闭包函数是指一个函数内部定义的函数，并且内部函数可以访问外部函数的局部变量或者参数。在Python中，函数是一等对象，因此我们可以将函数作为返回值返回，这样就创建了一个闭包函数。

### 4.3.2 语法

```python
# 闭包函数
def outer_func(*args):
    print(f"this is args: {args}")
    def inner_func(**kwargs):
        print(f"this is kwargs: {kwargs}")
        print(f"this is outer_func de args:{args}")
    return inner_func
a = outer_func(*(1,2,3,4,5,6,7,8))
print("this is inner:",a(**{"name": "lisan", "age" : 18}))
# 结果
this is args: (1, 2, 3, 4, 5, 6, 7, 8)
this is kwargs: {'name': 'lisan', 'age': 18}
this is outer_func de args:(1, 2, 3, 4, 5, 6, 7, 8)
this is inner: None
```

## 4.4 装饰器

### 4.4.1 定义

- 装饰器是利用了函数的这些特性（**函数是一等对象、闭包和语法糖**），实现在不修改被装饰函数源代码的情况下，向函数添加额外功能的一种方式。
- 装饰器本质上是一个函数，它接受一个函数作为输入，并返回一个新的函数，这个新函数通常会包装原始函数并添加额外的功能。

### 4.4.2 语法

```python
# 装饰器(给函数附加功能)
def decorator(func):
    def cost_time(*args):
        start_time = time.time()
        func(args[0], args[1])
        end_time = time.time()
        print(f"cost time is : {end_time - start_time}")
    return cost_time

@decorator
def user_add(username, password):
    print(f"username: {username} and password: {password}")
    time.sleep(1)

user_add("lisi", 123456)
# 结果
username: lisi and password: 123456
cost time is : 19.870102167129517
```

## 4.5 内置函数

### 4.5.1 map函数（迭代执行）

```python
# 格式
map(func,*args)
# 举例
map(lamdba x : x + 3,range(1,10))
```

### 4.5.2 filter函数（过滤）

```python
# 格式
filter(func,*args)
# 举例
filter(lamdba x : x > 0,range(-3,3))
```

## 4.6 匿名函数（lambda）

### 4.6.1 定义

```yacas
匿名函数: 就是使用 lambda 关键字定义的函数

匿名函数只能书写一行代码。
匿名函数的返回值不需要 return, 一行代码(表达式) 的结果就是返回值。

使用场景: 作为函数的参数, 这个函数比较简单,值使用一次,没有必要使用 def 定义。
```

### 4.6.2 语法

```python
lambda 参数: 一行代码   # 这一行代码,称为是表达式

# 匿名函数一般不需要我们主动的调用, 一般作为函数的参数使用的
# 我们在学习阶段为了查看匿名函数定义的是否正确,可以调用
# 1, 在定义的时候 ,将匿名函数的引用保存到一个变量中
变量 = lambda 参数: 一行代码
fun = lambda x : print(x,'Hello')
# 调用lambda函数
fun(9)
```

# 5. 面向对象

## 5.1 面向过程与对象的区别

- 面向过程
  - 关注的是 具体步骤的实现, 所有的功能都自己书写
  - 亲力亲为
  - 定义一个个函数, 最终按照顺序调用函数
- 面向对象
  - 关注的是结果, 谁(对象) 能帮我做这件事
  - 偷懒
  - 找一个对象(), 让对象去做

## 5.2 类

### 5.2.1 定义

- 抽象的概念,  对 多个 特征和行为相同或者相似事物的统称
- 泛指的(指代多个,而不是具体的一个)

### 5.2.2 组成

1. 类名 (给这多个事物起一个名字, 在代码中 满足大驼峰命名法(每个单词的首字母大写))
2. 属性 (事物的特征, 即有什么, 一般文字中的名词)
3. 方法 (事物的行为, 即做什么事, 一般是动词)

### 5.2.3 特性

- **封装**

> 封装是指将**属性**和**操作方法**打包在一起，形成一个称为类的实体。<br>类通过公共接口提供对其内部实现细节的访问。<br>通过封装，我们可以隐藏具体实现的细节，只暴露必要的接口给外部使用。使用关键字`class`定义一个类来实现封装。

```python
class Animal:
    """动物类"""
    def __init__(self, name, age, fly):
        self.name = name
        self.age = age
        self.__fly = fly

    def eat(self, food):
        print(f"{self.name} is eating {food}")

    def __era(self):
        print(f"{self.name} 是私有方法!")
```

- **继承**

> 继承是指使用一个已经存在的类作为基础，从而创建一个新类。<br>新类可以继承原有类的属性和方法，并可以添加自己特定的属性和方法。<br>继承提供了重用代码的方式，子类可以重写父类的方法或者添加新的方法。使用关键字`class`后面跟上父类名称，实现继承。

```python
class Fish(Animal):
    """鱼类"""
    def __init__(self, name, age, color):
        super().__init__(name, age, fly="None")
        self.color = color

    def show(self):
        print(f"{self.name} is showing")
```

- **多态**

> 多态是指在不同的对象上执行相同的操作时，可以根据对象的类型执行不同的操作。<br>多态允许使用一致的方式处理不同类型的对象

### 5.2.4 语法

```python
class 类名:
    """定义添加属性的方法"""
    def __init__(self, name, age):  # 这个方法是创建对象之后调用
        self.name = name  # 给对象添加 name 属性
        self.age = age   # 给对象添加 age 属性
        
    """在缩进中书写的内容,都是类中的代码"""
    def 方法名(self):   # 就是一个方法
        pass 
```

## 5.3 魔法方法(内置函数)

### 5.3.1 __init\_\_ 方法

```yacas
1. 什么情况下自动调用
    > 创建对象之后会自动调用
2. 有什么用, 用在哪
    > 1. 给对象添加属性的, (初始化方法, 构造方法)   2. 某些代码, 在每次创建对象之后, 都要执行,就可以将这行代码写在 __init__ 方法
3. 书写的注意事项
    > 1. 不要写错了   2. 如果 init 方法中,存在出了 self 之外的参数, 在创建对象的时候必须传参
```

```python
class Cat:
    # 定义添加属性的方法
    def __init__(self, name, age):  # 这个方法是创建对象之后调用
        self.name = name  # 给对象添加 name 属性
        self.age = age   # 给对象添加 age 属性
```

### 5.3.2 _\_str__ 方法

```yacas
1. 什么情况下自动调用
    > 使用 print(对象) 打印对象的时候 会自动调用
2. 有什么用, 用在哪
    > 在这个方法中一般书写对象的 属性信息的, 即打印对象的时候想要查看什么信息,在这个方法中进行定义的
    > 如果类中没有定义 __str__ 方法,  print(对象) ,默认输出对象的引用地址
3. 书写的注意事项
    > 这个方法必须返回 一个字符串
```

```python
class Cat:
    # 定义添加属性的方法
    def __init__(self, n, age):  # 这个方法是创建对象之后调用
        self.name = n  # 给对象添加 name 属性
        self.age = age   # 给对象添加 age 属性
​
    def __str__(self):
        # 方法必须返回一个字符串, 只要是字符串就行,
        return f'小猫的名字是: {self.name}, 年龄是: {self.age}'
```

### 5.3.3 _\_del__ 方法

```yacas
__init__ 方法, 创建对象之后,会自动调用  (构造方法)
__del__ 方法, 对象被删除销毁时, 自动调用的(遗言, 处理后事)  (析构方法)
​
1. 调用场景, 程序代码运行结束, 所有对象都被销毁
2. 调用场景, 直接使用 del 删除对象(如果对象有多个名字(多个对象引用一个对象),需要吧所有的对象都删除才行 )
```

```python
class Demo:
    def __init__(self, name):
        print('我是 __init__, 我被调用了 ')
        self.name = name
​
    def __del__(self):
        print(f'{self.name} 没了, 给他处理后事...')
```

### 5.3.4 _\_dict__方法

```yacas
__dict__ 方法，查看实例化对象的所有属性
```

```python
class Demo:
    def __init__(self, name, age, grade):
        self.name = name
        self.age = age
        self.grade = grade
        
d1 = Demo('lisan', 12, 1)
print(d1.__dict__) # 展示d1的所有属性
```

## 5.4 装饰器

### 5.4.1 类方法@classmethod

```python
class Demo:
    @classmethod
    dfe met(cls) # cls -> 类本身
    	print(cls)
        
Demo.met()
# 结果
<class '__main__.Work'>
```

### 5.4.2 静态方法@staticmethod

```python
class Demo:
    @staticmethod
    def method():
        return 1
    
print('Data is :', Demo.metod())
```

## 5.5 cls 和 self 的区别（知识点）

```yacas
cls和self的区别?
答：cls是类本身的引用，例如：<class '__main__.Work'>。
self是类实例化对象的引用，例如：<__main__.Work object at 0x0000013B1BFCF3D0>
Student # Student -> 是cls的引用
s1 = Student() # s1 -> 是Student的实例化对象，s1-> self
```

# 6. 异常，模块，文件

## 6.1 异常

### 6.1.1 语法

```python
try:
    可能发生异常的代码
except 异常类型1:
    发生异常类型1执行的代码  
except Exception as 变量:  
    发生其他类型的异常,执行的代码
    # Exception是常见异常类的父类。Exception,可以捕获常见的所有异常, as 变量, 这个变量是一个异常类的对象, print(变量) 可以打印异常信息 
else:
    没有发生异常会执行的代码
finally: 
    不管有没有发生异常,都会执行的代码xxxxxxxxxx
```

## 6.2 模块

- #### 方式一

```python
import 模块名
# 使用模块中的内容
模块名.工具名
```

- #### 方式二

```python
from 模块名 import 工具名
# 使用
工具名  # 如果是函数和类需要加括号
```

- #### 方式三 [了解] 基本不用

```python
from 模块名 import *  
# 将模块中所有的内容都导入
```

## 6.3 文件

### 6.3.1 文件操作

- **方法一**

```python
# 1.打开文件
file_name = 'a.txt'
file = open(file_name, 'w', encoding='utf-8')
"""
参数 mode:  默认参数(r参数), 表示的是打开文件的方式 
        > r: read 只读打开
        > w: write  只写打开
        > a> append 追加打开, 在文件的末尾写入内容
"""
# 2.读文件
file.read()
file.readline()
# 3.写文件
file.write('好好学习\n')
# 4.关闭文件
file.close()
```

- **方法二**

```yacas
with open() 打开文件的好处: 不用自己去书写关闭文件的代码, 会自动进行关闭
```

```python
file_name = 'a.txt'
with open(file_name, 'r' , encoding='utf-8') as file:
    while True:
        buf = file.readline()
        if buf:  # if len(buf) != 0
            print(buf)
        else:
            break
```

### 6.3.2 json 文件的处理

- **json文件的读取**

```python
# 1, 导入 json
import json

# 2, 读打开文件
file_name = 'info.json'
with open(file_name, encoding='utf-8') as f:
    # 3. 读取文件
    result = json.load(f)
```

- **json文件的写入**

```python
# 1, 导入 json
import json

# 2, 读打开文件
file_name = 'info.json'
data = {
    "name": "Tom",
    "age": 20,
    "city": "New York"
}
with open(file_name, 'w' ,encoding='utf-8') as f:
    # 3. 读取文件
    result = json.dump(my_list, f, ensure_ascii=False, indent=4)
    
# ensure_ascii: 默认值为True，如果该选项为True，则所有非 ASCII 字符将转换成 Unicode 转义序列.
# indent: 如果指定了该参数，则使用指定的缩进间隔，使得输出更加易读.
```

# 7. Python 进阶知识

## 7.1 迭代器（Iterator）

> 迭代器是可以迭代的对象。<br>从技术上讲，Python **迭代器对象**必须实现两个特殊方法，__iter__()和__next__()统称为**迭代器协议**。

```python
lis = [1, 2, 3, 4]
lis_iter = iter(lis)
lis_iter.__next__() # 此操作会使得迭代索引➕1
for i in lis_iter:
    print(i)
```

## 7.2 生成器（Yield）

> Python生成器是创建迭代器的简单方法。我们上面提到的所有开销都由Python的生成器自动处理。<br>生成器是一个函数，它返回一个对象（迭代器），我们可以对其进行迭代（一次一个值）。

```python
def demo_yield(n):
    print('start')
    while n > 0:
        print('yiekd before')
        yield n				# 程序执行开始挂起，返回n值
        n -= 1
        print('after yield')
        
y = demo_yield(3)
print(y.__next__())	# 第一次调用
print(y.__next__())	# 第二次调用
print(y.__next__())	# 第三次调用
print(y.__next__())	# 超出调用
```


# 8. Python 模块

## 8.1 OS 模块

> **应用场景：**
>
> - 如果是读写文件的话，建议使用内置函数`open()`；
> - 如果是路径相关的操作，建议使用`os`的子模块`os.path`；
> - 如果要逐行读取多个文件，建议使用`fileinput`模块；
> - 如果要创建临时文件或路径，建议使用`tempfile`模块；
> - 如果要进行更高级的文件和路径操作则应当使用`shutil`模块。

| 方法                                    | 描述                                                         |
| :-------------------------------------- | ------------------------------------------------------------ |
| os.getcwd()                             | 获取当前工作目录（路径）                                     |
| os.chdir("dirname")                     | 改变当前工作路径，相当于**cd**命令                           |
| os.curdir                               | 返回当前目录                                                 |
| os.pardir                               | 返回当前目录的父级目录                                       |
| os.environ                              | 获取系统环境变量                                             |
| os.mkdir('dirname')                     | 生成单级目录，相当于mkdir。                                  |
| os.makedirs("dirname/dirname2")         | 递归创建目录                                                 |
| os.rmdir('dirname')                     | 删除单级空目录，若目录不为空则无法删除。                     |
| os.remove(path)                         | 删除一个文件                                                 |
| os.removedirs('dirname2')               | 若目录为空，则删除。并递归到上一级目录，如若为空，也删除，以此类推。 |
| os.rename('oldname', 'newname')         | 重命名文件/目录                                              |
| **os.listdir('dirname')**               | **列出指定目录下的所有文件和子目录，包含隐藏文件，并以列表方式打印。** |
| **os.path.join()**                      | 可以将多个传入路径组合为一个路径。                           |
| **os.path.abspath()**                   | 将传入路径规范化，返回一个相应的绝对路径格式的字符串。       |
| **os.path.basename()**                  | 该函数返回传入路径的“基名”，即传入路径的最下级目录。         |
| **os.path.dirname()**                   | 与上一个函数正好相反，返回的是最后一个分隔符前的整个字符串   |
| **os.path.split()**                     | 函数的功能就是将传入路径以最后一个分隔符为界，分成两个字符串，并打包成元组的形式返回. |
| **os.path.exists()**                    | 这个函数用于判断路径所指向的位置是否存在。若存在则返回`True`，不存在则返回`False`. |
| **os.path.isabs()**                     | 函数判断传入路径是否是绝对路径，若是则返回`True`，否则返回`False`。 |
| **os.path.isfile() 和 os.path.isdir()** | 函数分别判断传入路径是否是文件或路径                         |

## 8.2 PyMySQL

> 标准化数据库连接格式（后续会持续更新）

```mysql
# 创建数据库
create_database = """
CREATE DATABASE IF NOT EXISTS students
"""

# 选择数据库
choice_database = """
use students
"""

# 建表
create_users_table = """
CREATE TABLE IF NOT EXISTS users(
    id INT AUTO_INCREMENT,
    name TEXT NOT NULL,
    age INT,
    gender TEXT,
    PRIMARY KEY (id)
)
"""

# 插入记录，注意：`users`, `name`, `age`, `gender`，不是引号包裹，是键盘左上角的撇号
insert_users = """
INSERT INTO 
`users` (`name`, `age`, `gender`)
VALUES
('zhangsan',23,'male'),
('lisi',24,'female'),
('wangwu',25,'male');
"""

# 更改
update_users = """
UPDATE `users` set `name`='jack' where `name` = 'zhangsan'
"""

# 查询
select_users = "SELECT * FROM users"

# 删除记录
delete_user = """
DELETE from users where `name`="lisi"
"""

# 删除表
drop_table = "DROP table users"

# 删除数据库
drop_database = "DROP database students"
```

> Python-连接mysql数据库代码

```python
from pymysql import connect, Error

class Mysql:
    """初始化Mysql数据库"""
    def __init__(self, hostname, port, username, password):
        self.hostname = hostname
        self.port = port
        self.username = username
        self.password = password

    """创建数据库连接对象"""
    def conn_db(self):
        try:
            conn = connect(host=self.hostname, port=self.port, user=self.username, password=self.password)
            return conn
        except Error as e:
            print(e)

    """执行SQL语句"""
    def exec_sql(self, sql):
        connection = None
        try:
            connection = self.conn_db()
            with connection.cursor() as cursor:
                cursor.execute(create_database)
                cursor.execute(choice_database)
                cursor.execute(sql)
                if 'CREATE' in sql or 'INSERT' in sql or 'UPDATE' in sql or 'DELETE' in sql or 'DROP' in sql:
                    connection.commit()
                    print("数据操作成功！")
                elif 'SHOW' in sql or 'SELECT' in sql:
                    result = cursor.fetchall()
                    print(f"result:{result}")
                    print("数据库读取成功!")
        except Error as e:
            print(e)
        finally:
            connection.close()


if __name__ == '__main__':
    open_sql = Mysql(hostname='127.0.0.1', port=3306, username='root', password='123456')
    sqls = [create_users_table, insert_users, update_users, delete_user, drop_table, drop_database]
    for sql in sqls:
        open_sql.exec_sql(sql)
```

## 8.3 PyTest

### 8.3.1 args参数

| 参数 | 作用                                                         |
| :--: | ------------------------------------------------------------ |
|  -v  | 详细输出测试信息。                                           |
|  -q  | 简要输出测试信息。                                           |
|  -s  | 输出测试用例中的print语句。                                  |
|  -x  | 遇到失败用例时立即停止测试。                                 |
|  -k  | 根据条件指定用例去测试，如：'-k' ,'TestCase and not test_case_1'（可指定类名&函数名） |
|  -m  | 根据修饰器指定用例去测试，如：'-m' , 'skip'(可指定测试种类)  |

```python
# args参数运用
if __name__ == '__main__':
    pytest.main(args=['-q', '-s','pytest_fixture.py'(测试文件名)]) 
```

### 8.3.2 pytest-ordering

- **修饰器**：@pytest.mark.run(order=1)
- **说明**：order的值越小，测试用例越先被执行！
- **作用**：控制用例的执行顺序
- **依赖**：pytest-ordering 模块

```python
@pytest.mark.run(order=4)
def test_case_1(login):
    print('Test case 1')
    assert True
```

### 8.3.3 fixture（前置函数）

- **修饰器**：@pytest.fixture()
- **说明**：将被@pytest.fixture()修饰的方法传入测试用例参数
- **作用**：完成测试之前的初始化，也可以返回数据给测试函数。
- **参数一**：**scope**（**作用域参数**）

> 作用域:
>
> function 函数或者方法级别都会被调用.
>
> class 类级别调用一次.
>
> module 模块级别调用一次.
>
> session 是多个文件调用一次(可以跨.py文件调用，每个.py文件就是module).

```python
@pytest.fixture(scope='作用域')
def login():
    print('Login Operation!')
    
@pytest.mark.run(order=4)
def test_case_1(login):
    print('Test case 1')
    assert True
```

- **参数二**：**params**（**传递数据参数**）

> 测试过程中需要大量的测试数据，如果每条测试数据都编写一条测试用例，用例数量将是非常宠大的。
>
> 一般我们在测试过程中会将测试用到的数据以参数的形式传入到测试用例中，并为每条测试数据生成一个测试结果数据.

```python
@pytest.fixture(params=[1, 2, 3,])
def past_data(request):
    print(f"get data : {request.param}")
    return request.param

def test_case_9(past_data):
    print(f"past_data : {past_data}")
```

- **参数三**：**autouse**（**自动执行参数**）

> 如果每条测试用例都需要添加 fixture 功能，则需要在每一要用例方法里面传入这个fixture的名字.
>
> 这里就可以在装饰器里面添加一个参数 **autouse='true'**，它会自动应用到所有的测试方法中，只是这里没有办法返回值给测试用例.
>
> @pytest.fixture 里设置 autouse 参数值为 true（默认 false），每个测试函数都会自动调用这个前置函数

```python
@pytest.fixture(autouse=True)
def login():
    print('Login Operation!')
```

### 8.3.4 parametrize（参数化）

- **修饰器**：@pytest.mark.paramtrize(‘data’, param) 

- **Parametrize()**方法主要参数说明：

  **argsnames** ：参数名，是个字符串，如中间用逗号分隔则表示为多个参数名.

  **argsvalues** ：参数值，参数组成的列表，列表中有几个元素，就会生成几条用例.

- **作用**：实现测试数据的传参。
- **方式一**：**单次使用 parametrize**

```python
@pytest.mark.parametrize('x', [1, 2, 3])
def test_case_10(x):
    print(f"Parameters : {x}")
    assert True
```

- **方式二**：**多次使用 parametrize**

```python
# 参数按照 （笛卡尔积方式）组合
@pytest.mark.parametrize('x', [1, 2, 3])
@pytest.mark.parametrize('y', [4, 5, 6])
def test_case_10(x, y):
    print(f"Parameters : {x} and {y}")
    assert True
```

### 8.3.5 fixture **与** parametrize **结合**

- **适用场景**：测试数据需要在 **fixture** 方法中使用，同时也需要在测试用例中使用。可以在使用 **parametrize** 的时候添加一个参数 **indirect=True**，**pytest** 可以实现将参数传入到 **fixture** 方法中，也可以在当前的测试用例中使用.
- **结合方法**：**indirect** 参数设置为 **True**，**pytest** 会把 **argnames** 当作函数去执行，将 **argvalues** 作为参数传入到 **argnames** 这个函数里.

```python
@pytest.fixture()
def login_app(request):
    print("Login browser...")
    print(f"value is {request.param}")
    return request.param

@pytest.mark.parametrize('login_app', ['zhangsan', 'lisi', 'wangwu'], indirect=True)
def test_case_11(login_app):
    print("Test_case_11!")
    print(f"value is {login_app}")
    assert True
    
# 运行结果：
# Login browser...
# value is zhangsan（方法输出）
# Test_case_11!
# value is zhangsan（用例输出）
```

### 8.3.6 pyyaml（数据源）

- **适用场景**：在实际的测试工作中，通常需要对多组不同的输入数据，进行同样的测试操作步骤，以验证我们的软件质量。这种测试，在功能测试中非常耗费人力物力，但是在自动化中，却比较好实现，只要实现了测试操作步骤，然后将多组测试数据以数据驱动的形式注入，就可以实现了.当数据量非常大的时候，我们可以将数据存放到外部文件中，使用的时候将文件中的

- **依赖**：PyYaml 模块

```yaml
# 数据源
---
-
  - 张三
  - 李四
  - 王五
-
---
```

- **Python 代码**

```python
import yaml
file_name=os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)),'data/data.yaml'))
@pytest.mark.parametrize('x, y, z', yaml.safe_load(open(file_name, encoding='utf-8')))
def test_case_12(x, y, z):
    print("Testing case 12!")
    print(f"Value is {x} and {y} and {z}")
    assert True
    
# 运行结果：
# Testing case 12!
# Value is 张三 and 李四 and 王五
```

### 8.3.7 pytest-xdist（分布式测试）

- **介绍**：pytest-xdist 是 pytest 分布式执行插件，可以多个 CPU 或主机执行，这款插件允许用户将测试并发执行（进程级并发）, 插件是动态决定测试用例执行顺序的。
- **依赖**：pytest-xdist 模块

```python
# 参数 -n atuo（可以指定内核数）
if __name__ == "__main__":
    pytest.main(args=['-s', '-v', '-n', 'auto'])
```

### 8.3.8 **allure**（测试报告）

- **依赖**：allure-pytest 模块

```python
import pytest,os

allure_result_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'report/json'))
allure_report_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'report/html'))

if __name__ == "__main__":
    pytest.main(args=['-s', '-v', '--alluredir', allure_result_path])
    cmd = 'allure generate %s -o %s -c' % (allure_result_path, allure_report_path)
    os.system(cmd)
# 注意：
# 这里可能会有报错现象，解决方法：将pycharm软件设置成以管理员模式运行
```

## 8.4 selenium（web自动化测试）

### 8.4.1 selenium初始化（2种）

- **方式一**：

```python
# 导入包 （selenium 4版本）
import pytest, os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
# 用 os 处理路径
chrome_driver_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'drivers/chromedriver.exe'))
edge_driver_path = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'drivers/msedgedriver.exe'))
# 处理测试初始化
@pytest.fixture(scope='session')
def open_close_browser():
    print("Open browser!")
    # 调起 chrome 浏览器
    driver = webdriver.Chrome(service=Service(chrome_driver_path))
    # 调起 edge 浏览器
    # driver = webdriver.Edge(service=Service(edge_driver_path))
    yield driver
    print("Close browser!")
    driver.close()
    print("Test over!")
```

- **方式二**：（**推荐**）

```python
# 导入包 （selenium 4版本）
import pytest, os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from webdriver_manager.microsoft import IEDriverManager
from webdriver_manager.firefox import GeckoDriverManager
# 处理测试初始化
print("Open browser!")
    # 调起 chrome 浏览器
    # 设置 option 参数（选择）
    service = Service(executable_path=ChromeDriverManager().install())
    driver = webdriver.Chrome(service=service)
    # 调起 edge 浏览器
    # service = Service(executable_path=EdgeChromiumDriverManager().install())
    # driver = webdriver.Edge(service=service)
    # 调起 ie 浏览器
    # service = Service(executable_path=IEDriverManager().install())
    # driver = webdriver.Ie(service=service)
    # 调起 firefox 浏览器
    # service = Service(executable_path=GeckoDriverManager().install())
    # driver = webdriver.Firefox(service=service)
    yield driver
    print("Close browser!")
    driver.close()
    print("Test over!")
```

### 8.4.2 **chrome** **启动参数**（3种）

- **--headless**: 无界面运行（**示例**）

```python
option = webdriver.ChromeOptions()
option.add_argument('--headless')
driver = webdriver.Chrome(options=option, service=Service(chrome_driver_path))
```

- **--start-maximized**: 窗口最大化运行

```python
option.add_argument('--start-maximized')
```

- **--incognito**: 隐身模式运行

```python
option.add_argument('--incognito')
```

- 不展示“**浏览器正在被自动化程序控制**”提示

```python
option.add_experimental_option('excludeSwitches', ['enable-automation'])
```

### 8.4.3 八大定位方式

|          定位方式          | 代码                                                         |
| :------------------------: | ------------------------------------------------------------ |
|        **id 定位**         | driver.find_element(By.ID, 'kw')                             |
|       **name 定位**        | driver.find_element(By.NAME, 'wd')                           |
|     **classname 定位**     | driver.find_element(By.CLASS_NAME, 's_ipt')                  |
|     **tar_name 定位**      | driver.find_element(By.TAG_NAME, 'input')                    |
|        **css 定位**        | 1. driver.find_element(By.CSS_SELECTOR, '#kw') # CSS选择器<br>2. driver.find_element(By.CSS_SELECTOR, 'input.s_plt') # CSS选择器 |
|       **xpath 定位**       | driver.find_element(By.XPATH, '//*[@id="kw"]')               |
|     **link_text 定位**     | driver.find_element(By.LINK_TEXT, '新闻')                    |
| **partial_link_text 定位** | driver.find_element(By.PARTIAL_LINK_TEXT, '新')              |

#### 8.4.3.1 css 定位

**2.3.1** **工具获取**

- 通过chrome开发者工具复制可得， 百度首页为例,css定位搜索输入框

```python
driver.find_element(By.CSS_SELECTOR, '#kw').send_keys('selenium')
```

**2.3.2** **手工编写获取**

- 标签和属性组合： 标签名[属性名=属性值]

- 有多个标签时，标签之前空格相隔

- 属性可以是任意属性

- class/id除了可以采用以上写法外，还可以采用专用写法, 以百度搜索输入框为例：

- - 通过class编写： 标签名.class属性值

  ```python
  input.s_ipt 	# input标签中class属性值为s_ipt
  ```

  - 通过id编写： 标签名#id属性值

  ```python
  input#kw 		# input标签中id属性值为kw
  ```

  - class 和 id 组合

  ```python
  input#kw.s_ipt 	# input标签中id值为kw,class值为s_ipt
  ```

#### 8.4.3.2 xpath 定位

| 表达式   | 描述                         |
| -------- | ---------------------------- |
| **//**   | **从当前节点获取子孙节点**   |
| **/**    | **从当前节点获取直接子节点** |
| **@**    | **选取属性**                 |
| **[ ]**  | **添加筛选条件**             |
| *        | 获取当前节点                 |
| **       | 获取当前节点的父节点         |
| nodename | 获取该节点的所有子节点       |

- **XPath**：即为XML路径语言（XML Path Language），它是一种用来确定XML文档中某部分位置的语言。它被开发者采用来当作小型查询语言.

- 获取xpath路径有两种方式：

  - **通过开发者工具复制 xpath 路径**：

  ```python
  driver.find_element(By.XPATH, '//*[@id="kw"]')
  ```

  - **手工编写 xpath 路径**：

   **1.纯标签路径**：

  纯标签路径分两种：一种是绝对路径，另一种是相对路径， 以百度首页输入框为例：

  ​	1. 绝对路径：

  ```python
  /html/body/div/div/div[5]/div/div/form/span[1]/input
  ```

  2. 相对路径：

  ```python
  //form/span[1]/input
  ```

  **2.标签和属性相结合**：

  **语法**：//标签名[@属性名=属性值]

  ```python
  //input[@id='kw']		# 查找input标签中属性id值为kw的页面元素
  //*[@id='kw'] 			# 查找属性为id=‘kw'的页面元素
  //span/input[@id='kw'] 	# 标签也可以有多个
  //span[@class='bg s_ipt_wr quickdelete-wrap']/input # 还可以使用上层标签的属性帮助定位
  ```

  **3. xpath 模糊匹配**：

  - starts-with: 以什么开头

  ```python
  //input[starts-with(@type, "sub")] # 查找input标签中type属性,其值以sub开头的页面元素
  ```

  - ends-with: 以什么结尾（注：chrome使用的是xpath1.0版本，不支持此项模糊查询)

  ```python
  //input[ends-with(@type,"mit")] # 查找input标签中type属性,其属性值以mit结尾的页面元素
  ```

  - contains: 包含

  ```python
  //input[contains(@class, 's_btn')] # 查找input标签中class属性中含有“s_btn”字符串的页面元素
  ```

  - contains: 包含text

  ```python
  //div[contains(text(), "热榜")] # 查找div标签中文本中含有“热榜”的页面元素
  ```

#### 8.4.3.3 link_text 定位

- **link_text定位文字链接**：

```python
driver.find_element(By.LINK_TEXT, '新闻').click()
```

#### 8.4.3.4 partial_link_text 定位

- **partial_link_text通过部分文字定位文字链接**：

```python
driver.find_element(By.PARTIAL_LINK_TEXT, '新').click()
```

#### 8.4.3.5 relative 相对定位

- **to_left_of**：定位位于指定元素左侧的元素。
- **to_right_of**：定位位于指定元素右侧的元素。
- **above**：定位位于指定元素上方的元素。
- **below**：定位位于指定元素下方的元素。

```python
from selenium.webdriver.support.relative_locator import locate_with
# 获取相对元素
text_locator = locate_with(By.ID,'kw').to_left_of({By.ID: "kw"})
# 注意：
# 目前不知道这个元素能干什么，后续补充！
```

### 8.4.4 添加等待时间

#### 8.4.4.1 休眠等待：time（调式）

- 有时候为了保证脚本运行的稳定性，需要脚本中添加等待时间, 添加休眠：

```python
import time # 先导入模块文件
sleep(3) # 休眠3秒
```

#### 8.4.4.2 显式等待：Explicit Waits（常用）

- 显式等待主要是使用**WebDriverWait**来实现，其语法格式如下：

````python
from selenium.webdriver.support.wait import WebDriverWait

WebDriverWait(driver,timeout,poll_frequency=0.5,ignored_exceptions=None)

# driver：浏览器驱动；
# timeout：最长超时时间，默认以秒为单位；
# poll_frequency：检测的间隔步长，默认为0.5s；
# ignored_exceptions：超时后的抛出的异常信息，默认抛出NoSuchElementExeception异常。
````

在使用**WebDriverWait**时，需要搭配**until**或**until_not**方法来使用，其语法格式如下：

```python
until(method, message='')
until_not(method, message='')

# method：指定预期条件的判断方法；
# message：超时后抛出的提示；
```

- **示例**：

```python
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# 创建 Wait 对象
wait = WebDriverWait(driver,2,0.3)
wait.until(EC.title_is('标题'))
# 后续操作
```

| 方法                                                | 描述                                                         | 类型       |
| :-------------------------------------------------- | ------------------------------------------------------------ | ---------- |
| title_is('')                                        | 判断当前页面的 title 是否等于预期                            | 布尔       |
| title_contains('')                                  | 判断当前页面的 title 是否包含预期字符串                      | 布尔       |
| presence_of_element_located(locator)                | 判断元素是否被加到了 dom 树里，并不代表该元素一定可见        | WebElement |
| visibility_of_element_located(locator)              | 判断元素是否可见，可见代表元素非隐藏，并且元素的宽和高都不等于0 | WebElement |
| visibility_of(element)                              | 跟上一个方法作用相同，但传入参数为 element                   | WebElement |
| text_to_be_present_in_element(locator ,'')          | 判断元素中的 text 是否包含了预期的字符串                     | 布尔       |
| text_to_be_present_in_element_value(locator ,‘’)    | 判断元素中的 value 属性是否包含了预期的字符串                | 布尔       |
| frame_to_be_available_and_switch_to_it(locator)     | 判断该 frame 是否可以 switch 进去，True 则 switch 进去，反之 False | 布尔       |
| invisibility_of_element_located(locator)            | 判断元素中是否不存在于 dom 树或不可见                        | 布尔       |
| element_to_be_clickable(locator)                    | 判断元素中是否可见并且是可点击的                             | 布尔       |
| staleness_of(element)                               | 等待元素是否从 dom 树中移除                                  | 布尔       |
| element_to_be_selected(element)                     | 判断元素是否被选中,一般用在下拉列表                          | 布尔       |
| element_selection_state_to_be(element,True)         | 判断元素的选中状态是否符合预期，参数 element，第二个参数为 True/False | 布尔       |
| element_located_selection_state_to_be(locator,True) | 跟上一个方法作用相同，但传入参数为 locator                   | 布尔       |
| alert_is_present()                                  | 判断页面上是否存在 alert                                     | alert      |

- 示例代码：

```python
from time import sleep
from selenium import webdriver
from selenium.webdriver import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
 
driver = webdriver.Chrome()
driver.get('https://www.baidu.com/')
element = WebDriverWait(driver, 2,0.5,ignored_exceptions=None).until(EC.presence_of_element_located((By.ID, 'toolbar-search-input')),message='超时!') # 定位不存在的标签
element.send_keys('NBA',Keys.ENTER)
sleep(5)    	# 休眠5秒
driver.quit()   # 关闭浏览器并释放进程资源
```

#### 8.4.4.3 隐式等待：Implicit Waits

- **适用场景**：一般在 Browser 初始化阶段（conftest文件中）使用。

```python
driver.implicitly_wait(30)

# 隐式地等待一个元素被发现或一个命令完成，这个方法每次会话只需要调用一次。
```

### 8.4.5 浏览器操作（WebDriver方法）

#### 8.4.5.1 获取信息

- 获取浏览器信息，其方法分别如下所示：

```python
driver = webdriver.Chrome()
driver.title  		# 获取浏览器当前页面的标签
driver.current_url  # 获取浏览器当前地址栏的URL
driver.page_source  # 获取当前html源码
driver.name  		# 获取浏览器名称(chrome)
driver.current_window_handle	# 窗口句柄
driver.window_handles			# 当前窗口的所有句柄
driver.get_window_rect() 		# 获取浏览器尺寸，位置
driver.get_window_position() 	# 获取浏览器位置(左上角)

# 运行结果：
{'width': 1552, 'height': 840}
{'x': -8, 'y': -8}
```

#### 8.4.5.2 导航

- 浏览器导航，其实现方法分别为：

```python
driver.forward() 	# 浏览器前进
driver.back() 		# 浏览器后退
driver.refresh() 	# 刷新必应网页
```

#### 8.4.5.3 切换框架（iframe）

- **使用 WebElement**：

```python
# 存储网页元素
iframe = driver.find_element(By.CSS_SELECTOR, "#modal > iframe")

# 切换到选择的 iframe
driver.switch_to.frame(iframe)

# 单击按钮
driver.find_element(By.TAG_NAME, 'button').click()
```

- **使用 name 或 id**：

```python
# 通过 id 切换框架
driver.switch_to.frame('buttonframe')

# 单击按钮
driver.find_element(By.TAG_NAME, 'button').click()
```

- **使用索引**：

```python
# 基于索引切换到第 2 个 iframe
iframe = driver.find_elements(By.TAG_NAME,'iframe')[1]

# 切换到选择的 iframe
driver.switch_to.frame(iframe)
```

- **离开框架**：

```python
# 切回到默认内容
driver.switch_to.default_content()
```

#### 8.4.5.4 切换窗口（window）

```python
# 打开新闻 Tab 标签
driver.find_element(By.LINK_TEXT,'新闻').click()
# 休眠 1 秒
sleep(1)
# 切换到第一个 Tab 标签
driver.switch_to.window(driver.window_handles[0])
```

#### 8.4.5.5 屏幕截图

| 方法                                    | 描述                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| driver.save_screenshot(filename)        | 获取当前屏幕截图保存为指定文件，filename指定为完整保存路径或文件名 |
| driver.get_screenshot_as_base64()       | 获取当前的屏幕截图 base64 编码字符串                         |
| driver.get_screenshot_as_file(filename) | 获取当前的屏幕截图，使用完整的路径                           |
| driver.get_screenshot_as_png()          | 获取当前屏幕截图的二进制文件数据                             |

- 获取当前屏幕截图的案例：

```python
from time import sleep, strftime, localtime, time
# 文件路径名
filename = os.path.abspath(os.path.dirname(__file__)) + '/screenshot.png'
filename = 'screenshot.com'
# 图片的时间信息
st = strftime("%Y-%m-%d-%H-%M-%S", localtime(time()))
filename = st + '.png'
# 获取屏幕截图
driver.save_screenshot(filename)
```

- 获取截屏保存到文件夹中：

```python
import os 
from time import sleep, strftime, localtime, time
# 图片的时间信息
st = strftime("%Y-%m-%d-%H-%M-%S", localtime(time()))
file_name = st + '.png'
# 路径
path = os.path.abspath('screenshot')
file_path = path + '/' + file_name
# 获取屏幕截图
driver.save_screenshot_as_file(file_path)
```

### 8.4.6 元素操作（WebElement方法）

| 方法                    | 描述             |
| ----------------------- | ---------------- |
| text                    | 获取元素文本内容 |
| click()                 | 单击左键         |
| send_keys()             | 输入内容         |
| clear()                 | 清除内容         |
| submit()                | 提交表单         |
| get_attribute("属性")   | 获取属性值       |
| is_selected()           | 是否被选中       |
| is_enabled()            | 是否可用         |
| is_displayed()          | 是否显示         |
| value_of_css_property() | css 属性值       |

#### 8.4.6.1 鼠标操作（click）

- 常用的鼠标操作方法如下：

| 方法              | 描述                             |
| ----------------- | -------------------------------- |
| click()           | 单击左键                         |
| context_click()   | 单击右键                         |
| double_click()    | 双击左键                         |
| drag_and_drop()   | 鼠标拖动                         |
| move_to_element() | 鼠标悬停                         |
| execute_script()  | 界面滑动                         |
| perform()         | 执行所有ActionChains中存储的动作 |

- 单击左键

```python
from selenium.webdriver.common.by import By

element = driver.find_element(By.ID, 'su') #定位元素
element.click() # 给元素 单击左键
```

- 单击右键

- **依赖**：**pyautogui**第三方支持右键菜单元素操作

```python
import pyautogui
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
#定位元素
element = driver.find_element(By.ID, 'su') 
# 给元素 单击右键
ActionChains(driver).context_click(element).perform() 
# 等待2s
sleep(2)
# 
pyautogui.typewrite(['down', 'down', 'return'])
```

- 双击左键

```python
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
# 定位元素
element = driver.find_element(By.ID, 'su') 
# 给元素 双击左键
ActionChains(driver).double_click(element).perform() 
```

- 鼠标拖动

```python
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains

# 定位要拖动的元素
source = driver.find_element('选择器','xxx')
# 定位目标元素
target = driver.find_element('选择器','xx')
# 执行拖动动作
ActionChains(driver).drag_and_drop(source, target).perform()
```

- 鼠标悬停

```python
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
#定位元素
element = driver.find_element(By.XPATH, '"//div/a[@class='s-bri c-font-normal c-color-t']"') 
# 给元素 鼠标悬停
ActionChains(driver).move_to_element(element).perform() 
```

#### 8.4.6.2 键盘操作（send_keys）

- **作用**：模拟键盘输入内容。

- 在webdriver中的keys类中，提供了很多按键方法，常用的按键操作有：

| 操作             | 描述     |
| ---------------- | -------- |
| Keys.ENTER       | 回车键   |
| Keys.BACK_SPACE  | 删除键   |
| Keys.F1          | F1键     |
| Keys.SPACE       | 空格     |
| Keys.CONTROL     | Ctrl键   |
| Keys.TAB         | Tab键    |
| Keys.ESCAPE      | ESC键    |
| Keys.ALT         | Alt键    |
| Keys.SHIFT       | Shift键  |
| Keys.ARROW_UP    | 向上箭头 |
| Keys.ARROW_RIGHT | 向右箭头 |
| Keys.ARROW_DOWN  | 向下箭头 |
| Keys.ARROW_LEFT  | 向左箭头 |

- 回车键操作案例：

```python
from selenium.webdriver.common.by import By
from selenium.webdriver import Keys
# 定位元素
element = driver.find_element(By.ID, 'kw') 
# 给元素 双击左键
element.send_keys('selenium', Keys.ENTER)
```

- 复制，粘贴操作案例：

```python
from selenium.webdriver.common.by import By
from selenium.webdriver import Keys
# 定位元素
element = driver.find_element(By.ID, 'kw') 
# 全选文本
element.send_keys(Keys.CONTROL, 'a')
element.send_keys(Keys.CONTROL, 'c')
element.send_keys(Keys.CONTROL, 'v')
```

#### 8.4.6.3 其它操作（4种）

- 清除输入框的内容：**clear()**

```python
from selenium.webdriver.common.by import By
# 定位搜索输入框，向搜索输入框中输入文本“selenium"
element = driver.find_element(By.ID, 'kw').send_keys('selenium')  
# 等待2s
sleep(2) 
# 清除搜索输入框中的文本
element.clear() 
```

- 提交表单：**submit()**

```python
from selenium.webdriver.common.by import By
# 定位搜索输入框，向搜索输入框中输入文本“selenium"
driver.find_element(By.ID, 'kw').send_keys('selenium')  
#定位搜索按钮，并提交表单
driver.find_element(By.ID, 'su').submit() 
```

- 获取元素文本内容：**text**

```python
from selenium.webdriver.common.by import By
# 定位文本框
element = driver.find_element(By.CSS_SELECTOR, '#bottom_layer > div > p:nth-child(3) > a')
# 等待2s
sleep(1)
# 打印出元素文本内容
print('get text:', element.text)
```

- 获取页面元素属性：**get_attribute("属性")**

```python
from selenium.webdriver.common.by import By
# 定位文本输入框
element = driver.find_element(By.ID, 'kw')
# 打印文本输入框的 name 属性
print(f'the attribute value of element is: {element.get_attribute("name")}')
```

### 8.4.7 下拉框操作（Select）

| 方法/属性                  | 方法/属性描述  |
| -------------------------- | -------------- |
| select_by_value()          | 根据值选择     |
| select_by_index()          | 根据索引选择   |
| select_by_visible_text()   | 根据文本选择   |
| deselect_by_value()        | 根据值反选     |
| deselect_by_index()        | 根据索引反选   |
| deselect_by_visible_text() | 根据文本反选   |
| deselect_all()             | 反选所有       |
| options                    | 所有选项       |
| all_selected_options       | 所有选中选项   |
| first_selected_option      | 第一个选中选项 |

- 根据值选择案例：

```python
from selenium.webdriver.support.select import Select
# 定位下拉框
element = driver..find_element_by_name('selects')
# 选择选项
Select(element).select_by_visible_text('北京')
```

### 8.4.8 提示框操作（Prompt box）

- **alert 框**： 只有一个确定按钮。

```python
# 定位 alert 框
driver.find_element(By.ID, 'alert').click()
# 切换到 alert 框
alert = driver.switch_to.alert
# 点击确认
alert.accept()
```

- **confirm 框**  ：两个按钮，一个“**确定**”按钮，另一个是“**取消**”按钮。

```python
# 定位 confirm 框
driver.find_element(By.ID, 'confirm').click()
# 切换到 confirm 框
confirm = driver.switch_to.alert
# 点击确认
confirm.accept()
# 点击取消
confirm.dismiss()
```

- **prompt 框**： 一个文本输入框，一个”**确定**“按钮和一个”**取消**“按钮。

```python
# 定位 prompt 框
driver.find_element(By.ID, 'prompt').click()
# 切换到 prompt 框
prompt = driver.switch_to.alert
# 向 prompt 框发送消息
prompt.send_keys('lisainfsad')
# 点击确认
prompt.accept()
# 点击取消
prompt.dismiss()
```

### 8.4.9 执行 JS 脚本

- 界面滑动案例：

```python
# 使用execute_script方法执行JavaScript代码来实现鼠标滚动
js='window.scrollTo(0, 500);' 
# 向下滚动 500 像素
driver.execute_script(js) 
```

### 8.4.10 PO(page object) 框架

![](https://gitee.com/li-jiagong/imgs/raw/master/Selenium-imgs/PO(page%20object)%20.png)